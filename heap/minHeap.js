/*
Implement as an array.
i-th childs are:
 (i * 2) + 1
 (i * 2) + 2

parent index of i node:
  Math.ceil(i/2) - 1;

0 - 1 2 - 3 4 5 6 - 7 8 9 10 11 12 13 14

Heap insert should place element last in heap, then heapUp.
Heap delete should remove element in place then heap down fron top.
Heap get only min

could do a get some specific value to practice

There are minHeap and maxHeap

This is min heap, smallest element should be always the root.
*/

class minHeap {

    constructor () {
        this.heap = [];
    }

    getMin () {
        return this.heap[0];
    }

    insert (n) {
        this.heap.push(n);

        // heapUp
        for (let currentIndex = this.heap.length - 1; currentIndex > 0; currentIndex = Math.ceil( currentIndex / 2 ) - 1) {
            const parentIndex = Math.ceil( currentIndex / 2 ) - 1;

            if (this.heap[parentIndex] <= this.heap[currentIndex] ) {
                return this.heap;
            } else {
                // Swap
                [ this.heap[currentIndex], this.heap[parentIndex] ] = [ this.heap[parentIndex], this.heap[currentIndex] ];
            }
        }
        return this.heap;
    }

    delete () {
        //redo
    }

}