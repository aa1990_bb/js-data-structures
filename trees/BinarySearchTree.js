/*
let bst = new BinarySearTree()
bst.insert(10)
bst.insert(20)
bst.insert(30)
bst.insert(15)
bst.insert(5)
bst.insert(4)
bst.insert(6)
console.log('PreOder')
bst.showPreOrder(bst.root)
console.log('InOder')
bst.showInOrder(bst.root)
console.log('PostOder')
bst.showPostOrder(bst.root)
bst.delete(15,bst.root)
bst.showPreOrder(bst.root)
*/

class BinarySearTree {
    constructor () {
        this.root = null;
    }

    showPreOrder (node = null) {
        if (node) {
            console.log(node.value);
            this.showPreOrder(node.leftChild);
            this.showPreOrder(node.rightChild);
        }
    }

    showInOrder (node = null) {
        if (node) {
            this.showPreOrder(node.leftChild);
            console.log(node.value);
            this.showPreOrder(node.rightChild);
        }
    }

    showPostOrder (node = null) {
        if (node) {
            this.showPreOrder(node.leftChild);
            this.showPreOrder(node.rightChild);
            console.log(node.value);
        }
    }

    insert(value, node = null) {
        const newNode = {
            value,
            rightChild: null,
            leftChild: null,
        }
        if (node === null) {
            if (this.root) {
                this.insert(value, this.root);
            } else {
                this.root = newNode;
                return true;
            }
        } else {
            if (value < node.value && node.leftChild) {
                this.insert(value, node.leftChild)
            } else if (value < node.value && node.leftChild === null) {
                node.leftChild = newNode;
                return true;
            } else if (value > node.value && node.rightChild) {
                this.insert(value, node.rightChild)
            } else if (value > node.value && node.rightChild === null) {
                node.rightChild = newNode;
                return true;
            } else {
                return false;
            }
        }
    }

    getMinValue(node) {
        if (node.leftChild) {
            return getMinValue(node.leftChild)
        } else {
            return node.value;
        }
    }

    delete(value, node, parent = null) {
        if (this.root === null) {
            return false;
        } else {
            if (node === null) {
                return false;
            } else {
                if (value < node.value) {
                    this.delete(value, node.leftChild, node);
                } else if (value > node.value) {
                    this.delete(value, node.rightChild, node);
                } else {
                    // Cases: No child
                    if (node.leftChild === null && node.rightChild === null) {
                        if (parent === null) {
                            this.root = null;
                        } else if (value === parent.leftChild.value) {
                            parent.leftChild = null;
                        } else if (value === parent.rightChild.value) {
                            parent.rightChild = null;
                        }
                    } else if (node.leftChild && node.rightChild === null) {
                    // Cases: One child
                        if (parent === null) {
                            this.root = node.leftChild;
                        } else if (parent.leftChild.value === value) {
                            parent.leftChild = node.leftChild;
                        } else if (parent.rightChild.value === value) {
                            parent.rightChild = node.leftChild;
                        }
                        node = null;
                    } else if (node.rightChild && node.leftChild === null) {
                        if (parent === null) {
                            this.root = node.rightChild;
                        } else if (parent.leftChild.value === value) {
                            parent.leftChild = node.rightChild;
                        } else if (parent.rightChild.value === value) {
                            parent.rightChild = node.rightChild;
                        }
                        node = null;
                    } else if (node.leftChild && node.rightChild) {
                    // Case: Two childs
                        const newRootValue = this.getMinValue(node.rightChild);
                        node.value = newRootValue;
                        this.delete(newRootValue, node.rightChild, node);
                    }   
                }
            }
        }
    }
}